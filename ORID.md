# Daily Report (2023/07/12)


## O 
 
1. Code review

     Code review is the activity of development within the team.
     In this section, I learned a lot of tips from other students' homework. The most important thing is that other students will look at my code and provide me with modification suggestions, such as improper function naming, code redundancy, etc
2. Stream API
    1. Stream Creation
    2. Intermediate Operation
    3. Terminal Operation
3. OO
    1. Encapsulation: It is information hiding, hiding the essence of an object, so that users no longer pay attention to those details. It provides some external interfaces for others to use, just like the interior of a TV has been sealed, without knowing which parts it is composed of or how it works, just knowing to use a remote control to control it;
    2. Inheritance: is a major feature of object-oriented programming languages, which refers to the ability to use all the functions of existing classes and extend them without the need to rewrite the original class;
    3. Polymorphism: refers to the subclass being the parent class, where a reference to the parent type can point to the object of the subclass. The rewriting, overloading, and dynamic connection of methods constitute polymorphism;

## R
Meaningful
## I
I think today's course is excellent,the most meaningful aspect of this activity is code review.
## D
Code review.