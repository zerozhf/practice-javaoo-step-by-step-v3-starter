package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private int leaderId;
    private Person person ;
    public Klass(int number) {
        this.number = number;
    }

    public int getLeaderId() {
        return leaderId;
    }


    public int getNumber(){
        return number;
    }

    public void assignLeader(Student student){
        if(student.getKlass() == null){
            System.out.println("It is not one of us.");
            return;
        }
        leaderId = student.id;
        System.out.println(String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",student.name,student.age,number));


        if(person == null){
            return;
        }
        if(student.getKlass().equals(this)){
            person.say(student.name,number);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Klass)) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void attach(Person person){
        this.person = person;
    }

    public boolean isLeader(Student student){
        return student.id == leaderId;
    }
}
