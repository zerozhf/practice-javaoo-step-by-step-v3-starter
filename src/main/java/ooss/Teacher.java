package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klassList = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce(){
        if(klassList.size() == 0 ){
            return String.format("My name is %s. I am %d years old. I am a teacher.",name,age);
        }
        List<String> klassNumber = new ArrayList<>();
        for (Klass klass : klassList) {
            klassNumber.add(String.valueOf(klass.getNumber()));
        }
        return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.",name,age,String.join(", ",klassNumber));
    }

    public void assignTo(Klass klass){
        if(klassList.contains(klass)) return;;
        klassList.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return klassList.contains(klass);
    }

    public boolean isTeaching(Student student){
        return klassList.contains(student.getKlass());
    }

    public void say(String name,int number){
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.",this.name,number,name));
    }


}



