package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce(){
        if(klass == null){
            return String.format("My name is %s. I am %d years old. I am a student.",name,age);
        }
        if(klass.getLeaderId() == id){
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.",name,age,klass.getNumber());
        }
        return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.",name,age,klass.getNumber());
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass){
        return this.klass != null && this.klass.getNumber() == klass.getNumber();
    }

    public Klass getKlass() {
        return klass;
    }

    public void say(String name,int number){
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.",this.name,number,name));
    }


}
